/**
  ******************************************************************************
  * @file    stm32f4xx_nucleo_144.c
  * @author  MCD Application Team
  * @brief   This file provides set of firmware functions to manage:
  *          - LEDs and push-button available on STM32F4XX-Nucleo-144 Kit 
  *            from STMicroelectronics
  *          - LCD, joystick and microSD available on Adafruit 1.8" TFT LCD 
  *            shield (reference ID 802)
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 
  
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_nucleo_144.h"
#include "spi.h"


/** @defgroup BSP BSP
  * @{
  */ 

/** @defgroup STM32F4XX_NUCLEO_144 STM32F4XX NUCLEO 144
  * @{
  */   
    
/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL STM32F4XX NUCLEO 144 LOW LEVEL
  * @brief This file provides set of firmware functions to manage Leds and push-button
  *        available on STM32F4xx-Nucleo Kit from STMicroelectronics.
  * @{
  */ 

/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_TypesDefinitions STM32F4XX NUCLEO 144 LOW LEVEL Private TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_Defines STM32F4XX NUCLEO 144 LOW LEVEL Private Defines
  * @{
  */ 

/**
  * @brief STM32F4xx NUCLEO BSP Driver version number V1.0.3
  */
#define __STM32F4xx_NUCLEO_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __STM32F4xx_NUCLEO_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __STM32F4xx_NUCLEO_BSP_VERSION_SUB2   (0x03) /*!< [15:8]  sub2 version */
#define __STM32F4xx_NUCLEO_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */ 
#define __STM32F4xx_NUCLEO_BSP_VERSION        ((__STM32F4xx_NUCLEO_BSP_VERSION_MAIN << 24)\
                                             |(__STM32F4xx_NUCLEO_BSP_VERSION_SUB1 << 16)\
                                             |(__STM32F4xx_NUCLEO_BSP_VERSION_SUB2 << 8 )\
                                             |(__STM32F4xx_NUCLEO_BSP_VERSION_RC))   

/**
  * @brief LINK SD Card
  */
#define SD_DUMMY_BYTE            0xFF
#define SD_NO_RESPONSE_EXPECTED  0x80

/**
  * @}
  */ 

/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_Macros STM32F4XX NUCLEO 144 LOW LEVEL Private Macros
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_Variables STM32F4XX NUCLEO 144 LOW LEVEL Private Variables
  * @{
  */ 


/**
 * @brief BUS variables
 */

#ifdef ADAFRUIT_TFT_JOY_SD_ID802
#ifdef HAL_SPI_MODULE_ENABLED
uint32_t SpixTimeout = NUCLEO_SPIx_TIMEOUT_MAX; /*<! Value of Timeout when SPI communication fails */
//static SPI_HandleTypeDef hnucleo_Spi; 
#endif /* HAL_SPI_MODULE_ENABLED */

#ifdef HAL_ADC_MODULE_ENABLED

#endif /* HAL_ADC_MODULE_ENABLED */
#endif /* ADAFRUIT_TFT_JOY_SD_ID802 */

/**
  * @}
  */ 

/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_FunctionPrototypes STM32F4XX NUCLEO 144 LOW LEVEL Private FunctionPrototypes
  * @{
  */
#ifdef ADAFRUIT_TFT_JOY_SD_ID802

#ifdef HAL_SPI_MODULE_ENABLED
//static void SPIx_Init(void);
static void SPIx_Write(uint8_t Value);
static void SPIx_Error(void);
//static void SPIx_MspInit(SPI_HandleTypeDef *hspi);

/* SD IO functions */
void SD_IO_Init(void);
void SD_IO_CSState(uint8_t state);
void SD_IO_WriteReadData(const uint8_t *DataIn, uint8_t *DataOut, uint16_t DataLength);
uint8_t SD_IO_WriteByte(uint8_t Data);


#endif /* HAL_SPI_MODULE_ENABLED */

#ifdef HAL_ADC_MODULE_ENABLED

#endif /* HAL_ADC_MODULE_ENABLED */

#endif /* ADAFRUIT_TFT_JOY_SD_ID802 */

/**
  * @}
  */ 

/** @defgroup STM32F4XX_NUCLEO_144_LOW_LEVEL_Private_Functions STM32F4XX NUCLEO 144 LOW LEVEL Private Functions
  * @{
  */ 

/**
  * @brief  This method returns the STM32F4xx NUCLEO BSP Driver revision
  * @retval version: 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __STM32F4xx_NUCLEO_BSP_VERSION;
}





/******************************************************************************
                            BUS OPERATIONS
*******************************************************************************/
#ifdef ADAFRUIT_TFT_JOY_SD_ID802

/******************************* SPI ********************************/
#ifdef HAL_SPI_MODULE_ENABLED

/**
  * @brief  Initializes SPI MSP.
  */
//static void SPIx_MspInit(SPI_HandleTypeDef *hspi)
//{
//  GPIO_InitTypeDef  GPIO_InitStruct;  
//  
//  /*** Configure the GPIOs ***/  
//  /* Enable GPIO clock */
//  NUCLEO_SPIx_SCK_GPIO_CLK_ENABLE();
//  NUCLEO_SPIx_MISO_MOSI_GPIO_CLK_ENABLE();
//  
//  /* Configure SPI SCK */
//  GPIO_InitStruct.Pin = NUCLEO_SPIx_SCK_PIN;
//  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
//  GPIO_InitStruct.Pull  = GPIO_PULLUP;
//  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
//  GPIO_InitStruct.Alternate = NUCLEO_SPIx_SCK_AF;
//  HAL_GPIO_Init(NUCLEO_SPIx_SCK_GPIO_PORT, &GPIO_InitStruct);

//  /* Configure SPI MISO and MOSI */ 
//  GPIO_InitStruct.Pin = NUCLEO_SPIx_MOSI_PIN;
//  GPIO_InitStruct.Alternate = NUCLEO_SPIx_MISO_MOSI_AF;
//  GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
//  HAL_GPIO_Init(NUCLEO_SPIx_MISO_MOSI_GPIO_PORT, &GPIO_InitStruct);
//  
//  GPIO_InitStruct.Pin = NUCLEO_SPIx_MISO_PIN;
//  GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
//  HAL_GPIO_Init(NUCLEO_SPIx_MISO_MOSI_GPIO_PORT, &GPIO_InitStruct);

//  /*** Configure the SPI peripheral ***/ 
//  /* Enable SPI clock */
//  NUCLEO_SPIx_CLK_ENABLE();
//}

/**
  * @brief  Initializes SPI HAL.
  */
//static void SPIx_Init(void)
//{
//  if(HAL_SPI_GetState(&hnucleo_Spi) == HAL_SPI_STATE_RESET)
//  {
//    /* SPI Config */
//    hnucleo_Spi.Instance = NUCLEO_SPIx;
//    /* SPI configuration contraints
//          - ST7735 LCD SPI interface max baudrate is 15MHz for write and 6.66MHz for read
//            Since the provided driver doesn't use read capability from LCD, only constraint 
//            on write baudrate is considered.
//          - SD card SPI interface max baudrate is 25MHz for write/read
//       to feat these constraints SPI baudrate is set to:
//	      - For STM32F412ZG devices: 12,5 MHz maximum (PCLK2/SPI_BAUDRATEPRESCALER_8 = 100 MHz/8 = 12,5 MHz)
//		  - For STM32F446ZE/STM32F429ZI devices: 11,25 MHz maximum (PCLK2/SPI_BAUDRATEPRESCALER_8 = 90 MHz/8 = 11,25 MHz)
//    */ 
//    hnucleo_Spi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
//    hnucleo_Spi.Init.Direction = SPI_DIRECTION_2LINES;
//    hnucleo_Spi.Init.CLKPhase = SPI_PHASE_2EDGE;
//    hnucleo_Spi.Init.CLKPolarity = SPI_POLARITY_HIGH;
//    hnucleo_Spi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
//    hnucleo_Spi.Init.CRCPolynomial = 7;
//    hnucleo_Spi.Init.DataSize = SPI_DATASIZE_8BIT;
//    hnucleo_Spi.Init.FirstBit = SPI_FIRSTBIT_MSB;
//    hnucleo_Spi.Init.NSS = SPI_NSS_SOFT;
//    hnucleo_Spi.Init.TIMode = SPI_TIMODE_DISABLED;
//    hnucleo_Spi.Init.Mode = SPI_MODE_MASTER;

//    SPIx_MspInit(&hnucleo_Spi);
//    HAL_SPI_Init(&hnucleo_Spi);
//  }
//}

/**
  * @brief  SPI Write a byte to device
  * @param  DataIn: value to be written
  * @param  DataOut: value to read
  * @param  DataLegnth: length of data
  */
static void SPIx_WriteReadData(const uint8_t *DataIn, uint8_t *DataOut, uint16_t DataLegnth)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_SPI_TransmitReceive(&hspi3, (uint8_t*) DataIn, DataOut, DataLegnth, SpixTimeout);
    
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    SPIx_Error();
  }
}

/**
  * @brief  SPI Write a byte to device.
  * @param  Value: value to be written
  */
static void SPIx_Write(uint8_t Value)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t data;

  status = HAL_SPI_TransmitReceive(&hspi3, (uint8_t*) &Value, &data, 1, SpixTimeout);
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    SPIx_Error();
  }
}

/**
  * @brief  SPI error treatment function
  */
static void SPIx_Error (void)
{
  /* De-initialize the SPI communication BUS */
  HAL_SPI_DeInit(&hspi3);
  
  /* Re-Initiaize the SPI communication BUS */
  //SPIx_Init();
}

/******************************************************************************
                            LINK OPERATIONS
*******************************************************************************/

/********************************* LINK SD ************************************/
/**
  * @brief  Initializes the SD Card and put it into StandBy State (Ready for 
  *         data transfer).
  */
void SD_IO_Init(void)
{
  
  uint8_t counter;
//GPIO_InitTypeDef  GPIO_InitStruct;
//  /* SD_CS_GPIO Periph clock enable */
//  SD_CS_GPIO_CLK_ENABLE();

//  /* Configure SD_CS_PIN pin: SD Card CS pin */
//  GPIO_InitStruct.Pin = SD_CS_PIN;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_PULLUP;
//  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
//  HAL_GPIO_Init(SD_CS_GPIO_PORT, &GPIO_InitStruct);
	// Cube mx ten yazdirdim


  

  /*------------Put SD in SPI mode--------------*/
  /* SD SPI Config */
  //SPIx_Init();
  
  /* SD chip select high */
  SD_CS_HIGH();
  
  /* Send dummy byte 0xFF, 10 times with CS high */
  /* Rise CS and MOSI for 80 clocks cycles */
  for (counter = 0; counter <= 9; counter++)
  {
    /* Send dummy byte 0xFF */
    SD_IO_WriteByte(SD_DUMMY_BYTE);
  }
}

/**
  * @brief  Set the SD_CS pin.
  * @param  val: pin value.
  */
void SD_IO_CSState(uint8_t val)
{
  if(val == 1) 
  {
    SD_CS_HIGH();
  }
  else
  {
    SD_CS_LOW();
  }
}

/**
  * @brief  Write a byte on the SD.
  * @param  DataIn: byte to send.
  * @param  DataOut: byte to read
  * @param  DataLength: length of data
  */
void SD_IO_WriteReadData(const uint8_t *DataIn, uint8_t *DataOut, uint16_t DataLength)
{
  /* Send the byte */
  SPIx_WriteReadData(DataIn, DataOut, DataLength);
}

/**
  * @brief  Writes a byte on the SD.
  * @param  Data: byte to send.
  */
uint8_t SD_IO_WriteByte(uint8_t Data)
{
  uint8_t tmp;
  /* Send the byte */
  SPIx_WriteReadData(&Data,&tmp,1);
  return tmp;
}

/**
  * @brief  Wait for loop in ms.
  * @param  Delay in ms.
  */
void LCD_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}
#endif /* HAL_SPI_MODULE_ENABLED */



#endif /* ADAFRUIT_TFT_JOY_SD_ID802 */



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

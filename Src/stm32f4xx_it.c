/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "CANRoutines.h"
#include "RN2483_LoRa.h"
//#include "SdCard.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define RF_PACKET_CHECK_NUMBER  68
/*test libs*/
char text[255];
group_32 RfDatas[18];
char BatteryCounter = 0;
 uint32_t RfPayload =200;
uint8_t TimerPeriodCounter1=0; 
extern RN2483 LoRa_Transmitter;
extern char FileName[10];
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern CAN_HandleTypeDef hcan2;
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart1;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles TIM1 update interrupt and TIM10 global interrupt.
  */
void TIM1_UP_TIM10_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 0 */
	
  /* USER CODE END TIM1_UP_TIM10_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 1 */
	if(TimerPeriodCounter1 ==3)
	{
		RfDatas[0].data_u32 = (MC.BusVoltage*100.0f);
		RfDatas[1].data_u32 = (MC.BusCurrent*100.0f+12000);
		RfDatas[2].data_u32 = BMS_T.BMS_SoC ;
		RfDatas[3].data_u32 = BMS_T.MinCellVoltage;
		RfDatas[4].data_u32 = BMS_T.MaxCellVoltage;
		RfDatas[5].data_u32 = BMS.ErrorCode;
		RfDatas[6].data_u32 = MC.MotorTemp;
		RfDatas[7].data_u32 = RF_PACKET_CHECK_NUMBER;
		RfDatas[8].data_u32 = (SC.Group1Current_fp+SC.Group2Current_fp)*100+900;
		RfDatas[9].data_u32 = SC.MPPTTemp;
		RfDatas[10].data_u32 = RF_PACKET_CHECK_NUMBER;
		RfDatas[11].data_u32 = MC.VehicleVelocity_kmh +140 ;
		RfDatas[12].data_u32 = INS.Slope*100.0f+1000;
		RfDatas[13].data_u32 = SC.TTDistance;
    RfDatas[14].data_u32 = (float) 0 + 500.0f;	
		// Check this again there will be long process
		//one string to rule them all
		sprintf(text,"A%uB%uC%uD%uE%uF%uB%uC%uD%uE%uF%uB%uC%uD%uE%uF%uB",
		RfDatas[0].data_u32,RfDatas[1].data_u32,RfDatas[2].data_u32,RfDatas[3].data_u32,
		RfDatas[4].data_u32,RfDatas[5].data_u32,RfDatas[6].data_u32=MC.MotorTemp,RfDatas[7].data_u32,
		RfDatas[8].data_u32,RfDatas[9].data_u32,RfDatas[10].data_u32,
		RfDatas[11].data_u32,RfDatas[12].data_u32,RfDatas[13].data_u32,RfDatas[14].data_u32,RfPayload);
		RfPayload += strlen(text);
		sprintf(text,"A%uB%uC%uD%uE%uF%uB%uC%uD%uE%uF%uB%uC%uD%uE%uF%uB",
		RfDatas[0].data_u32,RfDatas[1].data_u32,RfDatas[2].data_u32,RfDatas[3].data_u32,
		RfDatas[4].data_u32,RfDatas[5].data_u32,RfDatas[6].data_u32=MC.MotorTemp,RfDatas[7].data_u32,
		RfDatas[8].data_u32,RfDatas[9].data_u32,RfDatas[10].data_u32,
		RfDatas[11].data_u32,RfDatas[12].data_u32,RfDatas[13].data_u32,RfDatas[14].data_u32,RfPayload );
		//	//sends the string to Frodoceiver
		RN2483_SendRFData(&LoRa_Transmitter,text,(uint16_t) strlen(text));
		TimerPeriodCounter1 = 0;
		RfPayload = 200;
	}
   
	//SdRecordData(text,FileName);
	TimerPeriodCounter1++;
  /* USER CODE END TIM1_UP_TIM10_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */
	
  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles CAN2 RX0 interrupts.
  */
void CAN2_RX0_IRQHandler(void)
{
  /* USER CODE BEGIN CAN2_RX0_IRQn 0 */

  /* USER CODE END CAN2_RX0_IRQn 0 */
  HAL_CAN_IRQHandler(&hcan2);
  /* USER CODE BEGIN CAN2_RX0_IRQn 1 */

  /* USER CODE END CAN2_RX0_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

#include "SdCard.h"

FATFS SDFatFs;  /* File system object for SD disk logical drive */
FIL MyFile;     /* File object */
char SDPath[4]; /* SD disk logical drive path */ 
static uint8_t buffer[_MAX_SS]; /* a work buffer for the f_mkfs() */
FRESULT res;      /* FatFs function common result code */	
uint32_t byteswritten;  	/* File write counts */
uint32_t bytesread;  
uint8_t rtext[55]; 
//char line1[] = "Hour;Minutes;Seconds;MilliSeconds;BusVoltage;BusCurrent;MPPTCurrent;SOC;MaxVoltage;MinVoltage;MotorTempeture;MPPTTemp;EmusMaxTemp;Distance;Velocity;Latitude;Longtitude;Slope;BatteryT1;BatteryT2;BatteryT3;BatteryT4;BatteryT5;BatteryT6;BatteryT7;BatteryT8\n" ;
char    line2[] = "\r\n 2021";
uint8_t SdCardErrorFlag;

void SdErrorHandler(void);

void SdConfig(char* FileName, char* FileHeader)
{
	sprintf(FileName, "%s.TXT", FileName);	   /*##-1- Link the SD disk I/O driver ########################################*/
	if(FATFS_LinkDriver(&SD_Driver, SDPath) == 0) 
	{
		/*##-2- Register the file system object to the FatFs module ##############*/
		if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
		{
			SdErrorHandler();
		}
		else    
		{
			/*##-4- Create and Open a new text file object with write access #####*/
			if(f_open(&MyFile, FileName, FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) 
			{
				/* 'STM32.TXT' file Open for write Error */
				SdErrorHandler();
			}
			else
			{
				strcat(FileHeader, line2);
				f_lseek(&MyFile,f_size(&MyFile));
				/*##-5- Write data to the text file ################################*/
				res = f_write(&MyFile, FileHeader, strlen(FileHeader), (void *)&byteswritten);

				if((byteswritten == 0) || (res != FR_OK))
				{
					/* 'STM32.TXT' file Write or EOF Error */
					SdErrorHandler();
				}
				else
				{
					/*##-6- Close the open text file #################################*/
					f_close(&MyFile);
					//            /*##-7- Open the text file object with read access ###############*/
					if(f_open(&MyFile, FileName, FA_READ) != FR_OK)
					{
						/* 'STM32.TXT' file Open for read Error */
						SdErrorHandler();
					}
					else
					{
						/*##-8- Read data from the text file ###########################*/
						res = f_read(&MyFile, rtext, sizeof(rtext), (UINT*)&bytesread);
						if((bytesread == 0) || (res != FR_OK)) /* EOF or Error */
						{
							/* 'STM32.TXT' file Read or EOF Error */
							SdErrorHandler();
						}
						else
						{
							/*##-9- Close the open text file #############################*/
							f_close(&MyFile);
						}
					} 
				}
			}
		}
	}
}

void SdRecordData(char* Data ,char* FileName)
{
	if(SdCardErrorFlag == 0)
	{
  char Data1[255]; 
		//				HAL_NVIC_DisableIRQ(TIM1_UP_TIM10_IRQn);
		//				HAL_NVIC_DisableIRQ(CAN2_RX0_IRQn);
		sprintf(Data1, "\r\n%s",Data);
		/*##-1- Link the SD disk I/O driver ########################################*/
		if(FATFS_LinkDriver(&SD_Driver, SDPath) == 0) 
		{
			/*##-2- Register the file system object to the FatFs module ##############*/
			if(f_mount(&SDFatFs, (TCHAR const*)SDPath, 0) != FR_OK)
			{
				/* FatFs Initialization Error */
				SdErrorHandler();
			}
			else
			{
				//								sprintf(File_Name, "%d.TXT", FileNo);
				/*##-4- Open a text file object with write access #####*/
				if(f_open(&MyFile, FileName, FA_WRITE) != FR_OK) 
				{
					/* 'STM32.TXT' file Open for write Error */
					SdErrorHandler();
				}
				else
				{
					f_lseek(&MyFile,f_size(&MyFile));
					/*##-5- Write data to the text file ################################*/
					res = f_write(&MyFile, Data1, strlen(Data1), (void *)&byteswritten);

					if((byteswritten == 0) || (res != FR_OK))
					{
						/* 'STM32.TXT' file Write or EOF Error */
						SdErrorHandler();
					}
					else
					{
						/*##-6- Close the open text file #################################*/
						f_close(&MyFile);

						/*##-7- Open the text file object with read access ###############*/
						if(f_open(&MyFile, FileName, FA_READ) != FR_OK)
						{
							/* 'STM32.TXT' file Open for read Error */
							SdErrorHandler();
						}
						else
						{
							/*##-8- Read data from the text file ###########################*/
							res = f_read(&MyFile, rtext, sizeof(rtext), (UINT*)&bytesread);

							if((bytesread == 0) || (res != FR_OK)) /* EOF or Error */
							{
								/* 'STM32.TXT' file Read or EOF Error */
								SdErrorHandler();
							}
							else
							{
								/*##-9- Close the open text file #############################*/
								f_close(&MyFile);
							}
						}
					}
				}
			}
		}
	}	 
	/*##-11- Unlink the SD disk I/O driver ####################################*/
	//		Second =GPSSeconds;
	FATFS_UnLinkDriver(SDPath);
	//		HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
	//		HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
}
void SdErrorHandler(void)
{
	
 SdCardErrorFlag=1;
}
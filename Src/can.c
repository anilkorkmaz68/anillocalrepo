/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    can.c
  * @brief   This file provides code for the configuration
  *          of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "can.h"

/* USER CODE BEGIN 0 */
#include "CANRoutines.h"
/* USER CODE END 0 */

CAN_HandleTypeDef hcan2;

/* CAN2 init function */
void MX_CAN2_Init(void)
{

  /* USER CODE BEGIN CAN2_Init 0 */

  /* USER CODE END CAN2_Init 0 */

  /* USER CODE BEGIN CAN2_Init 1 */

  /* USER CODE END CAN2_Init 1 */
  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = 10;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan2.Init.TimeSeg1 = CAN_BS1_2TQ;
  hcan2.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan2.Init.TimeTriggeredMode = DISABLE;
  hcan2.Init.AutoBusOff = DISABLE;
  hcan2.Init.AutoWakeUp = DISABLE;
  hcan2.Init.AutoRetransmission = DISABLE;
  hcan2.Init.ReceiveFifoLocked = DISABLE;
  hcan2.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN2_Init 2 */

  /* USER CODE END CAN2_Init 2 */

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(canHandle->Instance==CAN2)
  {
  /* USER CODE BEGIN CAN2_MspInit 0 */
    __HAL_RCC_CAN1_CLK_ENABLE();
  /* USER CODE END CAN2_MspInit 0 */
    /* CAN2 clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();
    __HAL_RCC_CAN2_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**CAN2 GPIO Configuration
    PB12     ------> CAN2_RX
    PB13     ------> CAN2_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* CAN2 interrupt Init */
    HAL_NVIC_SetPriority(CAN2_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
  /* USER CODE BEGIN CAN2_MspInit 1 */

  /* USER CODE END CAN2_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN2)
  {
  /* USER CODE BEGIN CAN2_MspDeInit 0 */
    __HAL_RCC_CAN1_CLK_DISABLE();
  /* USER CODE END CAN2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();
    __HAL_RCC_CAN2_CLK_DISABLE();

    /**CAN2 GPIO Configuration
    PB12     ------> CAN2_RX
    PB13     ------> CAN2_TX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_12|GPIO_PIN_13);

    /* CAN2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN2_RX0_IRQn);
  /* USER CODE BEGIN CAN2_MspDeInit 1 */

  /* USER CODE END CAN2_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
void CAN_Transmit_FakeIdPack1(void)
{

	canTempMess64.data_u16[0] = (uint16_t) ((MC.BusVoltage)*100.0f) ; // bara volt
	canTempMess64.data_u16[1] = (uint16_t) ((SC.Group1Current_fp + SC.Group2Current_fp)*100.0f + 2000.0f) ; 
	canTempMess64.data_u16[2] = (uint16_t) ((MC.BusCurrent*100.0f)+ 3000.0f) ; // bara akim

	TxHeader.StdId = 0x300;
	TxHeader.ExtId = 0x01;
	TxHeader.RTR   = CAN_RTR_DATA;
	TxHeader.IDE   = CAN_ID_STD;
	TxHeader.DLC   = 8;
	TxHeader.TransmitGlobalTime = DISABLE;
	
	HAL_CAN_AddTxMessage(&hcan2, &TxHeader, canTempMess64.data_u8, &TxMailbox);


}


void CAN_Transmit_FakeIdPack2(void)
{
	canTempMess64.data_u8[0] = (uint8_t) MC.MotorTemp ; // motor sicaklik
	canTempMess64.data_u8[1] = (uint8_t) SC.MaxTemp ; // mppt sicaklik
	canTempMess64.data_u8[2] = (uint8_t) BMS.MaxTemp; // batarya sicaklik
	canTempMess64.data_u16[2] =(uint16_t) (INS.Slope + 500.0f); // egim
	canTempMess64.data_u16[3] = (uint16_t) ((MC.VehicleVelocity_kmh)*10.0f)+9000;
	
	TxHeader.StdId = 0x301;
	TxHeader.ExtId = 0x01;
	TxHeader.RTR   = CAN_RTR_DATA;
	TxHeader.IDE   = CAN_ID_STD;
	TxHeader.DLC   = 8;
	TxHeader.TransmitGlobalTime = DISABLE;
	
	HAL_CAN_AddTxMessage(&hcan2, &TxHeader, canTempMess64.data_u8, &TxMailbox);

	
}

void CAN_Transmit_FakeIdPack3(void)
{
  canTempMess64.data_fp[0] = (float) INS.EKF.POS_Lat_d;
	canTempMess64.data_fp[1] = (float) INS.EKF.POS_Lon_d;

	
	TxHeader.StdId = 0x302;
	TxHeader.ExtId = 0x01;
	TxHeader.RTR   = CAN_RTR_DATA;
	TxHeader.IDE   = CAN_ID_STD;
	TxHeader.DLC   = 8;
	TxHeader.TransmitGlobalTime = DISABLE;
	
	HAL_CAN_AddTxMessage(&hcan2, &TxHeader, canTempMess64.data_u8, &TxMailbox);

}
/* USER CODE END 1 */

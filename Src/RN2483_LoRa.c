#include "RN2483_LoRa.h"

void RN2483_Pause(RN2483 *rn2483);
void RN2483_Reset(RN2483 *rn2483);
void RN2483_SetFrequency(RN2483 *rn2483,uint32_t Frequency);
void RN2483_SetSpreadingFactor(RN2483 *rn2483,uint8_t SpreadingFactor);
void RN2483_SetPowerMod(RN2483 *rn2483,uint8_t Power);
void RN2483_SetMod(RN2483 *rn2483,char* Mode);
void RN2483_SetBitRate(RN2483 *rn2483,uint32_t Bitrate);

void RN2483_Init(RN2483 *rn2483,UART_HandleTypeDef* UART, char* Mod, uint8_t Power, uint32_t Freq, uint32_t Bitrate, uint8_t SprFactor)
{
  strcpy(rn2483->Mod, Mod);
	rn2483->Frequency = Freq;
	rn2483->Bitrate = Bitrate;
	rn2483->OutputPower = Power;
	rn2483->SpreadingFactor = SprFactor;
	rn2483->UART = UART;

}


void RN2483_Config(RN2483 *rn2483)
{
	RN2483_Reset(rn2483);
	HAL_Delay(200);
	RN2483_Pause(rn2483);
	HAL_Delay(200);
	RN2483_SetMod(rn2483,rn2483->Mod);
	HAL_Delay(200);
	RN2483_SetPowerMod(rn2483,rn2483->OutputPower);
	HAL_Delay(200);
	RN2483_SetFrequency(rn2483,rn2483->Frequency);
	HAL_Delay(200);
	RN2483_SetBitRate(rn2483,rn2483->Bitrate);
	HAL_Delay(200);
	if(!strcmp(rn2483->Mod,"lora"))
	RN2483_SetSpreadingFactor(rn2483,rn2483->SpreadingFactor);
	HAL_Delay(200);
}


void RN2483_SendRFData(RN2483 *rn2483,char* data, uint16_t size)
{
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) "radio tx ",9,1000);	
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,size,1000);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) "\r\n",2,1000);
}

void RN2483_StayAsReciver(RN2483 *rn2483)
{
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) "radio rx 0\r\n",12,1000);
}

void RN2483_Pause(RN2483 *rn2483)
{
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) "mac pause\r\n",11,1000);
}

void RN2483_Reset(RN2483 *rn2483)
{
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) "sys reset\r\n",11,1000);
}

void RN2483_SetFrequency(RN2483 *rn2483,uint32_t Frequency)
{
	char data[26];
	sprintf(data,"radio set freq %d\r\n",Frequency);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,26,1000);
}

void RN2483_SetSpreadingFactor(RN2483 *rn2483,uint8_t SpreadingFactor)
{
	char data[19];
	sprintf(data,"radio set sf sf%d\r\n",SpreadingFactor);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,strlen(data),1000);
}

void RN2483_SetPowerMod(RN2483 *rn2483,uint8_t Power)
{
	char data[18];
	sprintf(data,"radio set pwr %d\r\n",Power);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,strlen(data),1000);
}

void RN2483_SetMod(RN2483 *rn2483,char* Mod)
{
	char data[20];
	sprintf(data,"radio set mod %s\r\n",Mod);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,strlen(data),1000);
}

void RN2483_SetBitRate(RN2483 *rn2483,uint32_t Bitrate)
{
	char data[27];
	sprintf(data,"radio set bitrate %d\r\n",Bitrate);
	HAL_UART_Transmit(rn2483->UART,(uint8_t*) data,strlen(data),1000);
}


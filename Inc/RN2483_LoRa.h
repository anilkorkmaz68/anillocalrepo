#include "usart.h"
#include "string.h"
#include "stm32f4xx_hal.h"


typedef struct
{
UART_HandleTypeDef* UART;
uint32_t Frequency;
uint8_t SpreadingFactor;
uint8_t OutputPower;
char Mod[4];
uint32_t Bitrate; 	
}RN2483;


void RN2483_Init(RN2483 *rn2483,UART_HandleTypeDef* UART, char* Mod, uint8_t Power, uint32_t Freq, uint32_t Bitrate, uint8_t SprFactor);
void RN2483_Config(RN2483 *rn2483);
void RN2483_SendRFData(RN2483 *rn2483,char* data, uint16_t size);
void RN2483_StayAsReciver(RN2483 *rn2483);

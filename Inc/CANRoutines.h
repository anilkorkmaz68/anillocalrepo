/**
  ******************************************************************************
  * File Name          : CAN.h
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************

  */
/* Define to prevent recursive inclusion -------------------------------------*/


/* Includes ------------------------------------------------------------------*/

#include "can.h"
/******************************************************************************/

/* CAN BASE ADDRESSES AND PACKET OFFSETS */
	 
// Motor controller CAN base address and packet offsets

#define	MC_CAN_BASE						0x0400
#define MC_ID									0x00
#define MC_STATUS							0x01
#define	MC_BUS								0x02
#define MC_VELOCITY						0x03
#define MC_PHASE							0x04
#define MC_V_VECTOR						0x05
#define MC_I_VECTOR						0x06
#define MC_BEMF								0x07
#define MC_RAIL_1							0x08
#define MC_RAIL_2							0x09
#define MC_FAN								0x0A
#define MC_TEMP1							0x0B
#define MC_TEMP2							0x0C
#define MC_TEMP3							0x0D
#define MC_CUMULATIVE					0x0E
#define MC_CONF_REQUEST			  0x12
#define MC_CONF_REPLY					0x13
#define MC_CONF_DATA_OUT			0x14
#define MC_CONF_DATA_IN				0x15

/**************************************************/

// Motor2 controller CAN base address and packet offsets

#define MC2_CAN_BASE				  0x430
#define MC2_ID			          0x00
#define MC2_STATUS				    0x01
#define MC2_BJS				        0x02
#define MC2_VELOCITY				  0x03
#define MC2_PHASE			        0x04
#define MC2_V_VECTOR		      0x05
#define MC2_I_VECTOR		      0x06
#define MC2_BEMF							0x07
#define MC2_RAIL_1						0x08
#define MC2_RAIL_2						0x09
#define MC2_FAN					  		0x0A
#define MC2_TEMP1      			  0x0B
#define MC2_TEMP2						  0x0C
#define MC2_TEMP3							0x0D
#define MC2_CUMULATIVE				0x0E

/**************************************************/

// Driver controls CAN base address and packet offsets
#define DC_CAN_BASE						0x500
#define DC_DRIVE					  	0x01
#define DC_POWER						  0x02
#define DC_RESET						  0x03
#define DC_SWITCH						  0x04

#define SW_CAN_BASE   			  0X200
#define SW_DRIVE_MEAS					0X00
#define SW_DRIVE_FLAGS				0X01
#define SW_OTHER_FLAGS				0X02

#define MOC_CAN_BASE   			  0X350
#define MOC_BUTTON_STATUS			0X00

/**************************************************/

// Relay Board1 CAN base address and packet offsets

#define RB1_CAN_BASE 					0x600
#define RB1_STATUS						0x01
#define RB1_ALTIMETER_DATA	  0X02

/**************************************************/

// Relay Board1 CAN base address and packet offsets

#define RB2_CAN_BASE 					0x620
#define RB2_STATUS						0x01
#define RB2_ALTIMETER_DATA	  0X02

/**************************************************/


// BMS CAN base address and packet offsets

//#define MS_CAN_BASE          0x151

#define BMS_CAN_BASE										0x0AAA0000
#define BMS_BATTERY_PACK_STATUS    			0x02    //Soc calculated by Togi BMS (cloumb counting)
#define BMS_OVERALL_PACK_VOLTAGES   		0X03
#define BMS_OVERALL_PACK_CURRENTS  			0X04
#define BMS_POWER_AND_ENERGY   					0X05
#define BMS_OVERALL_PACK_TEMPERATURES   0X06
#define BMS_BROADCAST_CELL_VOLTAGES_1	  0X08
#define BMS_BROADCAST_CELL_VOLTAGES_2	  0X09
#define BMS_BROADCAST_CELL_VOLTAGES_3	  0X0A
#define BMS_BROADCAST_CELL_VOLTAGES_4	  0X0B
#define BMS_BROADCAST_CELL_VOLTAGES_5	  0X0C
#define BMS_BROADCAST_CELL_VOLTAGES_6	  0X0D


// eski bms 
//#define BMS_CAN_CELL					0x01
//#define BMS_CAN_TEMP_MODULE   0X02
//#define BMS_SOC_CURRENT				0x05
//#define BMS_ERROR_FLAGS       0x07
//#define BMS_CAN_TEMP					0X08
//#define BMS_EVENT_NUMBER			0x133
//#define BMS_GROUPTEMP					0x0B
//#define BMS_VOLTAGE_GROUP1		0x20
//#define BMS_VOLTAGE_GROUP2		0x21
//#define BMS_VOLTAGE_GROUP3		0x22
//#define BMS_VOLTAGE_GROUP4		0x23
//#define BMS_VOLTAGE_GROUP5		0x24

// MPPT CAN base address and packet offsets

#define SC_CAN_BASE				  0x250
#define SC_GROUP_VOUT			  0x00
#define SC_GROUP_CURRENT		0x01
#define SC_TEMP_GROUP1      0x02
#define SC_TEMP_GROUP2			0x03
#define SC_GROUP2_CURRENT		0x04
#define SC_GROUP2_TEMP			0x05
#define SC_TTDistance       0x07
#define SC_THIRD_TEMP       0x09
#define SC_GROUP3_CURRENT		0X22

/**************************************************/

// INS CAN base address and packet offsets

#define SBG_CAN_BASE				0x100

#define SBG_ECAN_MSG_STATUS_03 0x02		 		
#define SBG_ECAN_MSG_EKF_EULER 0x32
#define SBG_ECAN_MSG_EKF_ORIENTATION_ACC 0x33
#define SBG_ECAN_MSG_EKF_POS   0x34
#define SBG_ECAN_MSG_EKF_ALTITUDE 0x35
#define SBG_ECAN_MSG_EKF_POS_ACC 0x36
#define SBG_ECAN_MSG_UTC_1     0x11
#define SBG_ECAN_MSG_GPS1_POS  0x75
#define SBG_ECAN_MSG_GPS1_POS_ACC  0x77
#define SBG_ECAN_MSG_GPS1_POS_ALT 0x76
#define SBG_ECAN_MSG_GPS1_VEL 0x71
#define SBG_ECAN_MSG_GPS1_VEL_ACC 0x72
#define SBG_ECAN_MSG_IMU_ACCEL 0x21
#define SBG_ECAN_MSG_IMU_DELTA_ANGLE 0x24
#define SBG_ECAN_MSG_IMU_GYRO 0x22
#define SBG_ECAN_MSG_MAG_1 0x51


#define SBG_ECAN_MSG_AUTO_SLIP_CURV 0x120
 
/**************************************************/

#define GetBitValue(data, bitindex)				((data & (1 << bitindex)) >> bitindex)

#define RADIANSTODEGREES 57.2957795

/* Function Prototypes ----------------------------------------------------------*/

void CAN_Send_ResetMC(void);
void CAN_Receive_BMSDatas(void);
void CAN_Receive_MCDatas(void);
void CAN_Receive_SCDatas(void);
void CAN_Receive_SWDatas(void);
void CAN_Receive_DCDatas(void);
void CAN_Receive_TelemetryDatas(void);
void CAN_Receive_MOCDatas(void);
void CAN_Receive_RB1Datas(void);
void CAN_Receive_RB2Datas(void);
void CAN_FilterandStartConfig(CAN_HandleTypeDef *hcan,uint32_t FilterBank);
void GPIO_Toggle_CAN1Led(void);
void CAN_Receive_INSDatas(void);
/**********************************************************************************/

/* Struct and Union Definitions --------------------------------------------------*/

typedef struct{
uint8_t akim_modu;
float Ref_Velocity; 
float Ref_Current; 
}DriverControls;

typedef struct
{
uint8_t HardwareOverCurrentFlag:1;
uint8_t SoftwareOverCurrentFlag:1;
uint8_t DC_BusOverVoltageFlag:1;
uint8_t BadMotorPositionFlag:1;
uint8_t WatchdogResetFlag:1;
uint8_t ConfigReadErrorFlag:1;
uint8_t UnderVoltageLockOutFlag:1;
uint8_t DesaturationFaultFlag:1;
uint8_t	MotorOverSpeedFlag:1;
}sMC_ErrorFlags;

typedef struct
{
uint8_t Group1Cell[8];
uint8_t Group2Cell[8];
uint8_t Group3Cell[8];
uint8_t Group4Cell[8];
uint8_t Group5Cell[5];
uint8_t IgnitionStatus;
uint8_t EventNumber;
uint8_t AverageVoltage; 
uint8_t MaxTemp;
uint8_t MinTemp;
uint8_t AverageTemp;
uint8_t EstimatedSOC;
uint8_t CurrentMSB;
uint8_t CurrentLSB;
uint8_t	ErrorCode;
uint8_t MinVoltagemV; 		
uint8_t MaxVoltagemV;
float MinVoltage; 		
float MaxVoltage;
double  Current;

}BatteryManegmentSystem;

typedef struct
{
	float CellVoltages[35];
	float MinCellVoltage; 		
	float MaxCellVoltage;
	float CellVoltageDelta;
	float TotalPackVoltage;
	float PackCurrent;
	float Power;
	float Energy;
	float MinCellTemp;
	float MaxCellTemp;
	float TempDelte;
	float MeanTemp;
	
	uint8_t BMS_SoC;

}BatteryManegmentSystemTogi;

typedef struct
{
uint8_t OutputVoltagePWM:1;
uint8_t MotorCurrentLimitFlag:1;
uint8_t VelocityLimitFlag:1;
uint8_t BusCurrentLimitFlag:1;
uint8_t BusVoltageUpperLimitFlag:1;
uint8_t BusVoltageLowerLimitFlag:1;
uint8_t IPMorMotorTemperatureLimitFlag:1;
uint8_t Reserved:1;		    
}sMC_LimitFlags;

typedef struct
{
uint8_t   Reserved:1;
uint8_t   NoCellCommunicationFlag:1;
uint8_t	  LeakageFlag:1;             
uint8_t		CellModuleOverHeatFlag:1;     
uint8_t		ChargeOverCurrentFlag:1; 
uint8_t		DischargeOverCurrentFlag:1;
uint8_t		OverVoltageFlag:1;         
uint8_t		UnderVoltageFlag:1;     
}BMS_DiagnosticFlags;
typedef struct
{
uint8_t   Reserved:1;
uint8_t   MotorOverSpeedFlag:1;
uint8_t	  Desaturationfault:1;             
uint8_t		RailUnderVoltageLockOutFlag:1;     
uint8_t		ConfigReadErrorFlag:1; 
uint8_t		WatchdogCausedLastResetFlag:1;
uint8_t		BadMotorPositionHallSequenceFlag:1;         
uint8_t		DCBusOverVoltageFlag:1;     
uint8_t		SoftwareOverCurrentFlag:1;     
}MC_DiagnosticFlags;


typedef struct
{
float Img;
float Real;
}
fComplexNum;

typedef struct
{
uint32_t SerialNumber;
uint8_t TritiumID[4];
sMC_ErrorFlags Errors;
sMC_LimitFlags Limits;
float BusCurrent;				//A
float BusVoltage;				//volts
float VehicleVelocity_ms;	//m/s
float VehicleVelocity_kmh; //km/s	
float MotorVelocity;			//rpm
float PhaseACurrent;			//A
float PhaseBCurrent;			//A
fComplexNum MotorVoltageVector;	//volts
fComplexNum MotorCurrentVector;	//A
fComplexNum MotorBackEMF;		//volts
float VoltageRail15V;			//volts
float VoltageRail1V65;			//volts
float FanSpeed;					//rpm
float FanDrive;					//percent
float HeatSinkTemp;				//celcius
float MotorTemp;				//celcius
float AirInletTemp;				//celcius
float ProcessorTemp;			//celcius
float AirOutletTemp;			//celcius
float CapacitorTemp;			//celcius
float DCBusAmpsHour;			//Ah
float Odometer;					//m
float WheelRadius;              //m
}sMotorController;

typedef struct
{
float ReferenceVelocity_fp;
float	Regenative_fp;
uint8_t M_ReferenceVelocity;
uint16_t M_Regen;	
// Flags
uint8_t CC_Flag;
uint8_t CC_Negative_Flag;
uint8_t CC_Positive_Flag;
uint8_t	Drive_Flag;		
uint8_t	Neutral_Flag;	 
uint8_t	Reverse_Flag; 
uint8_t	RightSignal_Flag;
uint8_t	LeftSignal_Flag;	
uint8_t	Solar_Flag;
uint8_t	Radio_Flag;
uint8_t	Emergency_Flag;
uint8_t	Controller_Flag;
uint8_t Status;
uint8_t Regen_Flag;
}SteeringWheel;
typedef struct
{
	float Group1Current_fp;
	float Group2Current_fp;	
	float MPPTTemp_fp;
	uint8_t MPPTTemp;
	uint32_t TTDistance;
	uint8_t Temp[10];
	uint8_t TempIn;
	uint8_t MaxTemp;
	uint8_t MinTemp;
	uint8_t AvarageTemp;
}SolarCircuit;

typedef struct
{
uint8_t HeadlightFlag;
uint8_t CoolerFlag;
uint8_t HornFlag;
uint8_t SafeLampFlag;
	
}MonitorCircuit;

typedef struct
{
uint8_t Status;
uint16_t Pressure;
uint16_t Temperature;
uint16_t Humidity;	
uint8_t BrakeFlag;

uint8_t AuxilaryFlag;
}RelayBoard;	

typedef struct
{
	uint8_t Year;
	uint8_t Month;
	uint8_t Day;
	uint8_t Hour;
	uint8_t Min;
	uint8_t Sec;
	uint16_t Micro_Sec;
}GPS_Time;

typedef struct
{
	double Pitch_Deg_d;
	double Pitch_Rad_d;
	double Roll_Deg_d;
	double Yaw_Deg_d;	
	float Pitch_Acc;
	float Roll_Acc;
	float Yaw_Acc;
	double POS_Lon_d;
	double POS_Lat_d;
	float POS_Lon_Acc;
	float POS_Lat_Acc;
	float Altitude;
	float Undulation;
}SBG_EKF;

typedef struct
{
	double POS_Lon_d;
	double POS_Lat_d;
	float POS_Altitude;
	float POS_Undulation;
	uint8_t Num_Sv;
	float Velocity_N;
	float Velocity_E;
	float Velocity_D;
	float Velocity_N_Acc;
	float Velocity_E_Acc;
	float Velocity_D_Acc;
	float POS_Lon_Acc;
	float POS_Lat_Acc;
	GPS_Time Time;
}SBG_GPS;

typedef struct
{
	float Accel_X;
	float Accel_Y;
	float Accel_Z;
	float Gyro_X;
	float Gyro_Y;
	float Gyro_Z;
	float Mag_X;
	float Mag_Y;
	float Mag_Z;
}SBG_IMU;

typedef struct 
{
	float Slope;
	uint32_t Solution_Status;
	uint8_t SOL_ATTITUDE_VALID;
	uint8_t SOL_HEADING_VALID;
	uint8_t SOL_VELOCITY_VALID;
	uint8_t SOL_POSITION_VALID;
	SBG_EKF EKF;
	SBG_GPS GPS;
	SBG_IMU IMU;
}SBG_INS;

typedef union _group_64
{
	float    data_fp[2];
	uint8_t  data_u8[8];
	uint16_t data_u16[4];
	int16_t  data_16[4];
	uint32_t data_u32[2];
	int32_t  data_32[2];
	double   data_d64;
} group_64;

typedef union _group_32
{
	float    data_fp;
	uint8_t  data_u8[4];
	uint16_t data_u16[2];
	uint32_t data_u32;
} group_32;		  

typedef union _group_16
{
	uint8_t  data_u8[2];
	uint16_t data_u16;
} group_16;

/**************************************************************************/


/* Externs ---------------------------------------------------------------*/

extern SteeringWheel SW;
extern SolarCircuit SC;
extern BatteryManegmentSystem BMS;
extern sMotorController MC;
extern MonitorCircuit MOC;
extern RelayBoard RB1;
extern RelayBoard RB2;
extern SBG_INS INS;
extern BatteryManegmentSystemTogi BMS_T;

extern CAN_TxHeaderTypeDef   TxHeader;
extern CAN_RxHeaderTypeDef   RxHeader;
extern uint8_t RX_Data[8];
extern uint32_t TxMailbox;
extern group_64 canTempMess64;
extern group_32 canTempMess32;

/**************************************************************************/


/************************  *****END OF FILE**** ***************************/

